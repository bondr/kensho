# Kensho
### by Shinobi Systems (https://shinobi.systems)

Automatically scan and proxy all IP cameras on a network.

<img src="https://cdn.shinobi.video/images/kensho/demo-1.jpg"/>

### Download Pre-Built

- Windows x64 : https://cdn.shinobi.video/tools/Kensho_0.1.8.exe

### How to Run on Command Line

Has been tested on Ubuntu 18.04 and Windows 10. Running on command line requires actions through API.

```
git clone https://gitlab.com/Shinobi-Systems/kensho.git kensho
cd kensho
npm install
npm run cmd
```

### How to build Electron App

Has been tested on Windows 10.

```
git clone https://gitlab.com/Shinobi-Systems/kensho.git kensho
cd kensho
npm install
npm run dist
```

### How to run the Electron App in Development Mode

Has been tested on Windows 10.

```
git clone https://gitlab.com/Shinobi-Systems/kensho.git kensho
cd kensho
npm install
npm start
```

### How does it work?

It searches for all devices on a network then checks to see if port 80 (`defaultHostPort`) or port 554 (`defaultRtspPort`) are open. Once devices are found Kensho will do a TCP proxy to the ports.

Here is an example of a scan result. You can see that devices with only HTTP ports have been proxied. You can disable this by adding `"proxyHTTP":false` to your conf.json.

```
{
   "currentlyScanning": false,
   "numberOf": {
      "devices": 4,
      "rtspDevices": 1,
      "httpDevices": 4
   },
   "scannedNetworks": {
      "25.8.9": {},
      "192.168.1": {
         "34": {
            "ip": "192.168.1.34",
            "hostname": null,
            "mac": "REMOVED",
            "vendor": "Shenzhen Baichuan Digital Technology Co., Ltd.",
            "openRtspPort": true,
            "openWebPort": true,
            "proxyRtspPort": 555,
            "proxyHttpPort": 81
         },
         "68": {
            "ip": "192.168.1.68",
            "hostname": null,
            "mac": null,
            "vendor": null,
            "openRtspPort": false,
            "openWebPort": true,
            "proxyHttpPort": 82
         },
         "211": {
            "ip": "192.168.1.211",
            "hostname": null,
            "mac": "REMOVED",
            "vendor": "Actiontec Electronics, Inc",
            "openRtspPort": false,
            "openWebPort": true,
            "proxyHttpPort": 83
         },
         "254": {
            "ip": "192.168.1.254",
            "hostname": null,
            "mac": "REMOVED",
            "vendor": "Routerboard.com",
            "openRtspPort": false,
            "openWebPort": true,
            "proxyHttpPort": 84
         }
      },
      "192.168.56": {}
   },
   "msg": "Visit '/scan.json' to begin scanning."
}
```

Navigate to the opened web port. Default is `9511`.


### Configuration with `conf.json`

You can modify the following options within `conf.json`. Below are the default values.

> `conf.json` does not exist by default. You can copy `conf.sample.json` to create it.

```
{
    "webPanelPort": 9511,
    "proxyHTTP": true,
    "proxyRTSP": true,
    "defaultRtspPort": 554,
    "defaultHostPort": 80,
    "defaultOnvifPort": 8000,
    "startingRtspPort": 555,
    "startingHostPort": 81,
    "startingOnvifPort": 8001,
    "vpnPptpServerName": "",
    "vpnPptpServerAddress": "",
    "vpnPptpServerUsername": "",
    "vpnPptpServerPassword": "",
    "vpnPptpServerRequireMppe128": false,
}
```

### API

| Route | Description | Options |
|---|---|---|
| `/` | Gets current status and list of proxied devices. |  |
| `/scan` | Begin scan. |  |
| `/scanResults` | Gets the same information as the main route. |  |
| `/interfaces` | Gets a list of your connected network controllers. This should show your active VPN Connections. |  |
| `/vpnCreate` | Create a VPN Connection. The options can be add via POST or query string (GET). | `domain`, `user`, `pass`, `name`, `enabled`, `type` |


#### Join us on Discord!
The community started around an NVR platform but chat about Kensho is welcome!
https://discordapp.com/invite/mdhmvuH/
