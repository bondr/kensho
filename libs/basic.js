module.exports = function(s,config){
    s.parseJSON = function(string){
        var parsed
        try{
            parsed = JSON.parse(string)
        }catch(err){

        }
        if(!parsed)parsed = string
        return parsed
    }
    s.stringJSON = function(json){
        try{
            if(json instanceof Object){
                json = JSON.stringify(json)
            }
        }catch(err){

        }
        return json
    }
    s.getPostData = function(req,target,parseJSON){
        if(!target)target = 'data'
        var postData = false
        if(req.query && req.query[target]){
            postData = req.query[target]
        }else{
            postData = req.body[target]
        }
        postData = s.parseJSON(postData)
        return postData
    }
}
