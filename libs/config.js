module.exports = function(s){
    var config
    try{
        config = require(s.mainDirectory + '/conf.json')
    }catch(err){
        if(err && err.toString().indexOf('Cannot find module') === -1){
            console.log(err.toString())
            console.log('Your conf.json syntax may be broken.')
        }
        console.log('Default configuration will be used.')
        config = {}
    }
    var defaultConfig = {
        "webPanelPort": 9511,
        "proxyHTTP": true,
        "proxyRTSP": true,
        "defaultRtspPort": 554,
        "defaultHostPort": 80,
        "defaultOnvifPort": 8000,
        "startingRtspPort": 555,
        "startingHostPort": 81,
        "startingOnvifPort": 8001,
        "vpnPptpServerName": "",
        "vpnPptpServerAddress": "",
        "vpnPptpServerUsername": "",
        "vpnPptpServerPassword": "",
        "vpnPptpServerRequireMppe128": false,
    }
    Object.keys(defaultConfig).forEach(function(configKey){
        if(config[configKey] === undefined)config[configKey] = defaultConfig[configKey]
    })
    s.configuration = config
    return config
}
