module.exports = function(s,config){
    const {app, BrowserWindow} = require('electron')
    let mainWindow
    s.isElectronApp = false
    if(app){
        s.isElectronApp = true
        const ejse = require('ejs-electron')
        function createWindow () {
          mainWindow = new BrowserWindow({
              width: 1280,
              height: 720,
              nodeIntegration: true,
              icon: __dirname + '/electron/assets/img/brand/favicon.png'
          })
          ejse.data('ejsData', {
              webPort: config.webPanelPort,
              config: config,
              isWindows: s.isWindows
          })
          mainWindow.loadFile(__dirname + '/electron/pages/index.ejs')
          // mainWindow.webContents.openDevTools()
          mainWindow.on('closed', function () {
            mainWindow = null
          })
        }

        app.on('ready', createWindow)

        app.on('window-all-closed', function () {
            if (process.platform !== 'darwin') {
              app.quit()
            }
        })

        app.on('activate', function () {
          if (mainWindow === null) {
            createWindow()
          }
        })
    }
    return mainWindow
}
